from datetime import datetime
startTime = datetime.now()
import matplotlib.pyplot as plt
from scipy.io import loadmat
import numpy as np
from sklearn.svm import LinearSVC
from argparse import ArgumentParser
from sys import exit
from sklearn.metrics import confusion_matrix
from pickle import dump
import os

from ml_funcs import err_rate,kfold_cross_val 

parser = ArgumentParser(description="test")
parser.add_argument("-data_dir",action="store",help='path/to/data_directory',required=True)
parser.add_argument("-problem",choices=['4a','4b'],action="store",help='which hw1 problem ?',required=True)
args = parser.parse_args()

spam= loadmat(os.path.join(args.data_dir,'spam_data.mat'))
Xtest= spam['test_data']
X,Y= spam['training_data'],spam['training_labels'][0,:]
assert(X.shape == (5172, 32))
assert(Y.shape == (5172,))
assert(Xtest.shape == (5857, 32))
#randomize training set
ind=np.arange(X.shape[0])
np.random.shuffle(ind)
n_val= 500 #pull validation set out
ind_val, ind_tr= ind[:n_val], ind[n_val:]
Xval,Yval= X[ind_val,:],Y[ind_val]
Xtr,Ytr= X[ind_tr,:],Y[ind_tr]
#p4 k-fold cross validation
if args.problem == '4a':
	#cross validate for diff values of C, pick best
	k=10
	ntrain= Xtr.shape[0]
	Cvals= np.logspace(-3,3,num=7)
	avgerr= np.zeros(len(Cvals))-1
	for cnt,C in enumerate(Cvals):
		print 'kfold cv on C= %g' % C
		err,junk= kfold_cross_val(C, Xtr,Ytr,k,ntrain)
		print 'err= ',err
		avgerr[cnt]= err.mean()
	print 'avg err= ',avgerr,'C_vals= ',Cvals
	plt.plot(Cvals,avgerr,'k-')
	plt.scatter(Cvals,avgerr,s=50,color='b')
	plt.xlabel("C (regularization parameter)")
	plt.ylabel('Error rate')
	plt.title('%d Training' % ntrain)
	plt.xscale('log')
	plt.savefig('p4_LinearSVC.png')
	ibest= np.where(avgerr == avgerr.min())[0][0]
	print 'lowest cross valid error= ',avgerr[ibest],'for C = ',Cvals[ibest]
if args.problem == '4b':
	#train for kaggle
	C=10
	ntrain= Xtr.shape[0]
	k=10
	err,clf= kfold_cross_val(C, Xtr,Ytr,k,ntrain)
	print 'cross val err= ',err.mean()
	#error rate on Validation set
	pred= clf['lsvc'].predict(Xval)
	print 'error rate on Validation set= ',err_rate(pred, Yval)
	#predict test set
	pred= clf['lsvc'].predict(Xtest)
	#save pred to file
	fout=open('kaggle_spam_C_%d_ntrain_%d.csv' %(C,ntrain),'w')
	fout.write('Id,Category\n')
	for cnt,p in enumerate(pred): fout.write('%d,%d\n'% (cnt+1,p))
	fout.close()

print 'done, took ',datetime.now() - startTime,'sec'
