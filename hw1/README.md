# Hw1 for CS 189/289A Intro Machine Learning

Linear SVMs to predict hand written digits and spam emails

## Run my code

* git pull https://github.com/kaylanb/introML-CS289A.git burleigh-hw1
* problems 1-3: *python digit_classifier.py -data_dir /path/to/dir/digit-dataset -problem 1and2 OR 3a OR 3b*
* problem 4: *python spam_classifier.py -data_dir /path/to/dir/spam-dataset -problem 4a OR 4b*
* help info: *python digit_classifier.py -h OR python spam_classifier.py -h*

## Required python packages

* python (2.7.11)
* matplotlib
* scipy
* numpy
* sklearn
* pickle
