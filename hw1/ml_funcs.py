import numpy as np
import matplotlib.pyplot as plt
from sklearn.svm import LinearSVC


def err_rate(pred_labels,true_labels):
    return (pred_labels != true_labels).sum().astype(float)/len(true_labels)

def benchmark(pred_labels,true_labels):
    wrong= np.where(pred_labels != true_labels)[0]
    return err_rate(pred_labels,true_labels), wrong

def plot_confusion_matrix(cm,cmap=plt.cm.Blues,fname='conf_matrix.png',title='Confusion matrix'):
	plt.imshow(cm, interpolation='nearest', cmap=cmap)
	plt.title(title)
	plt.colorbar()
	tick_marks = np.arange(10)
	plt.xticks(tick_marks, tick_marks.astype(str))
	plt.yticks(tick_marks, tick_marks.astype(str))
	plt.ylabel('True label')
	plt.xlabel('Predicted label')
	plt.savefig(fname)
	plt.close()

def kfold_indices(k,n_train):
    '''returns array of indices of shape (k,n_train/k) to grab the k bins of training data'''
    bucket_sz=int(n_train/float(k))
    ind= np.arange(k*bucket_sz) #robust for any k, even if does not divide evenly!
    np.random.shuffle(ind) 
    return np.reshape(ind,(k,bucket_sz))

def kfold_cross_val(C, Xtr,Ytr,k,ntrain):
	'''C is parameter varying'''
	assert(ntrain <= Xtr.shape[0])
	ind= kfold_indices(k,ntrain)
	err=np.zeros(k)-1
	keep_clf=dict(err=1.)
	for i in range(k):
		ival= ind[i,:]
		itrain=np.array(list(set(ind.flatten())-set(ival)))
		assert(len(list(set(ival) | set(itrain))) == len(ind.flatten()))
		#train
		lsvc = LinearSVC(C=C,fit_intercept=True)
		lsvc.fit(Xtr[itrain,:],Ytr[itrain])
		#get error for this kth sample
		pred= lsvc.predict(Xtr[ival,:])
		err[i],wrong= benchmark(pred, Ytr[ival])
		if err[i] <= keep_clf['err']: 
			keep_clf['lsvc']=lsvc
			keep_clf['err']=err[i]
	return err,keep_clf


