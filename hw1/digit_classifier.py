from datetime import datetime
startTime = datetime.now()
import matplotlib.pyplot as plt
from scipy.io import loadmat
import numpy as np
from sklearn.svm import SVC,LinearSVC
from argparse import ArgumentParser
from sys import exit
from sklearn.metrics import confusion_matrix
from pickle import dump
import os

from ml_funcs import err_rate,benchmark,plot_confusion_matrix,kfold_cross_val 

def multi_imshow(nrow,ncol,imgs,indices=None,fname='test.png',titles=None):
	assert(imgs.shape[0] == imgs.shape[1])
	if nrow == 1 or ncol == 1: raise ValueError
	fig,ax=plt.subplots(nrow,ncol,figsize=(10,10))
	plt.subplots_adjust(hspace=1) #wspace=1)
	cnt=0
	for r in range(nrow):
		for c in range(ncol):
			if indices is not None: ax[r,c].imshow(imgs[:,:,indices[cnt]],cmap='gray')
			else: ax[r,c].imshow(imgs[:,:,cnt],cmap='gray')
			if titles is None: ax[r,c].set_title('cnt= %d' % cnt)
			else: ax[r,c].set_title(titles[cnt])
			cnt+=1
	plt.savefig(fname)
	plt.close()

parser = ArgumentParser(description="test")
parser.add_argument("-data_dir",action="store",help='path/to/data_directory',required=True)
parser.add_argument("-problem",choices=['1and2','3a','3b'],action="store",help='which hw1 problem ?',required=True)
parser.add_argument("-plots",action="store",help='set to anythin to make additional plots',required=False)
parser.add_argument("-SVC",action="store",help='set to anything to use svm.SVC() instead of svm.LinearSVC classifier',required=False)
args = parser.parse_args()

tr= loadmat(os.path.join(args.data_dir,'train.mat'))
test= loadmat(os.path.join(args.data_dir,'test.mat'))
X= tr['train_images']
##index order of test TOTALLY different from train!?
Xtest= np.swapaxes(test['test_images'],0,2) #now they are the same!
assert(X.shape == (28,28,60000))
assert(Xtest.shape == (28,28,10000))
# n samples x d features
X=np.swapaxes(np.reshape(X,(28*28,60000)), 1,0)   
Y=tr['train_labels'][:,0]
Xtest= np.swapaxes(np.reshape(Xtest,(28*28,10000)), 1,0)
print "train_images shape= ",X.shape,"train_labels shape= ",Y.shape,'test images.shape= ',Xtest.shape
#radomly select training and validation sets, 10000 will be validation
ind=np.arange(60000)
np.random.shuffle(ind)
n_val= 10000
ind_val, ind_tr= ind[:n_val], ind[n_val:]
Xval,Yval= X[ind_val,:],Y[ind_val]
Xtr,Ytr= X[ind_tr,:],Y[ind_tr]
print "size training samples= ",Xtr.shape,Ytr.shape,"size validation samples= ",Xval.shape,Yval.shape
if args.plots:
	#sanity check with histogram
	kwargs=dict(range=(0,10),normed=False)
	#fig,axes=plt.subplots(1,2,sharex=True)
	#plt.subplots_adjust(hspace=0)
	#ax=axes.flatten()
	plt.hist(Yval,color='r',label='Validation', **kwargs)
	plt.hist(Ytr,color='k',label='Training',alpha=0.5,**kwargs)
	plt.xlabel('Classes 0-9')
	plt.ylabel('# of Samples Per Classification')
	plt.legend(loc=0)
	plt.savefig('p1_histograms.png')
	plt.close()
if args.problem == '1and2':
	#train on many subsets
	clf={}
	for n_tr in [100, 200,500,1000,2000,5000,10000]:
		Xtr_subset,Ytr_subset= Xtr[:n_tr,:],Ytr[:n_tr] 
		#SVC linear, libsvn
		if args.SVC: clf[str(n_tr)] = SVC(kernel='linear',C=1.,degree=1)
		else: clf[str(n_tr)] = LinearSVC(C=1.,fit_intercept=True)
		clf[str(n_tr)].fit(Xtr_subset,Ytr_subset)
		print "trained on ",Xtr_subset.shape,"out of",Xtr.shape,"training samples, val samples = ",Xval.shape
	#predict
	err,conf_mat={},{}
	for key in clf.keys():
		pred= clf[key].predict(Xval)
		err[key],wrong= benchmark(pred,Yval)
		conf_mat[key]= confusion_matrix(Yval,pred)
	#p1 error rate 
	for k,e in err.items():
		plt.scatter(k,e,s=50,c='k')
	for i in [0.3,0.1]: 
		plt.hlines(i,0.,float(max(err.keys())),colors='b',linestyles='dashed')
		plt.text(0,i,str((1-i)*100.)+' % Accuracy',va='bottom',ha='left')
	plt.xlabel('Number of Training Samples')
	plt.ylabel('Error Rate')
	plt.xlim(0,12000)
	if args.SVC: title= "SVC(kernel='linear',C=1.,degree=1)"
	else: title= 'LinearSVC(C=1.,fit_intercept=True)'
	plt.title(title)
	if args.SVC: plt.savefig('p1_SVC.png')
	else: plt.savefig('p1_LinearSVC.png')
	plt.close()
	#confusion matrices
	cm_normed_arr=np.zeros((10,10,len(clf.keys())))-1
	titles=[]
	for cnt,key in enumerate(clf.keys()):
		cm_normed_arr[:,:,cnt] = conf_mat[key].astype('float') / conf_mat[key].sum(axis=1)[:, np.newaxis]
		titles.append('%s training' % key)
		if args.SVC: fname= 'p2_conf_mat_train_%s_SVC.png' % key
		else: fname= 'p2_conf_mat_train_%s_LinearSVC.png' % key
		plot_confusion_matrix(cm_normed_arr[:,:,cnt],fname=fname,title='%s training' % key)
	#multi_imshow(2,2,cm_normed_arr,fname='cm_test.png',titles=titles)
#p3 k-fold cross validation
if args.problem == '3a':
	#cross validate for diff values of C, pick best
	k=10
	ntrain=10000
	Cvals= np.logspace(-3,3,num=7)
	avgerr= np.zeros(len(Cvals))-1
	for cnt,C in enumerate(Cvals):
		print 'kfold cv on C= %g' % C
		err,junk= kfold_cross_val(C, Xtr,Ytr,k,ntrain)
		print 'err= ',err
		avgerr[cnt]= err.mean()
	print 'avg err= ',avgerr,'C_vals= ',Cvals
	fout=open('cv_on_C_ntrain_%d.pickle' % ntrain,'w')
	dump((Cvals,avgerr),fout)
	fout.close()
	plt.plot(Cvals,avgerr,'k-')
	plt.scatter(Cvals,avgerr,s=50,color='b')
	plt.xlabel("C (regularization parameter)")
	plt.ylabel('Error rate')
	plt.title('%d Training' % ntrain)
	plt.xscale('log')
	if args.SVC: plt.savefig('p3_SVC.png')
	else: plt.savefig('p3_LinearSVC.png')
	ibest= np.where(avgerr == avgerr.min())[0][0]
	print 'lowest cross valid error= ',avgerr[ibest],'for C = ',Cvals[ibest]
if args.problem == '3b':
	#train for kaggle
	C=0.01
	ntrain=10000
	k=10
	err,clf= kfold_cross_val(C, Xtr,Ytr,k,ntrain)
	print 'cv avg err= ',err.mean(),'cv err= ',err
	print 'classifier err= ',clf['err']
	#error rate on Validation set
	pred= clf['lsvc'].predict(Xval)
	print 'error rate on Validation set= ',err_rate(pred, Yval)
	#predict test set
	pred= clf['lsvc'].predict(Xtest)
	#save pred to file
	fout=open('kaggle_pred_C_%d_ntrain_%d.csv' %(C,ntrain),'w')
	fout.write('Id,Category\n')
	for cnt,p in enumerate(pred): fout.write('%d,%d\n'% (cnt+1,p))
	fout.close()

print 'done, took ',datetime.now() - startTime,'sec'
